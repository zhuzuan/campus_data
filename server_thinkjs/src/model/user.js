module.exports = class extends think.Model {
    async login(param){
        return await this.where({username: param.username,password:param.password}).find();
    }
    async findByRegisterName(param){
        return await this.where({username:param.username}).find();
    }
    async register(param){
        let time = Date.now() - 8 * 60 * 60
        let info = {
            user_id: think.uuid('v1'),   //用户id 
            username: param.username,//用户名
            password: param.password,//密码
            user_createtime: time,//创建时间
            nickname: '该用户还没设置昵称',//昵称
            avatar: 'http://oss.guoang.xyz/morentouxiang.jpg',
            realstate: 1,
            user_state: 2,
            companystate: 1
        }
        return await this.add(info);
    }

    async getuser(param){
        return await this.where({user_id:param.user.uid}).find();
    }
};
