module.exports = class extends think.Model {
    async getwebactivitylist(param) {
        console.log(param.lable)
        let sql1 = ' select count(*) as count from activity where ispublic=1 or ispublic=0'
        let info1 = []
        return await this.query(sql1, info1)
    }

    async webgetwebactivitylist(param) {
        let pagesize = param.pagesize * 1
        let page = (param.page - 1) * pagesize
        let info = [pagesize, page]
        let sql = 'select * from activity,user where activity.user_id=user.user_id'
        if (param.lable != '') sql = `${sql} and activity.activity_lable='${param.lable}'`//有分类时
        sql = `${sql}  and (activity.ispublic=1 or activity.ispublic=0)`
        sql = `${sql} order by activity.activity_read_num desc`
        return await this.query(sql, info)
    }

    async activitycontentadd(param) {
        let sql = `update activity set activity_read_num=activity_read_num+1 where activity_id =${param.id}`
        return await this.query(sql)
    }

    async getactivitycontent(param) {
        let sql = `select * from activity,user where user.user_id=activity.user_id and activity.activity_id=${param.id}`
        return await this.query(sql)
    }

    //获取活动列表
    async webgetwebactivitylistcount(param) {
        let sql1 = ` select count(*) as count from activity where ispublic=1 or ispublic=0`
        return await this.query(sql1)
    }
    async webgetwebactivitylist(param) {
        let pagesize = param.pagesize * 1
        let page = (param.page - 1) * pagesize
        let sql = 'select * from activity,user where activity.user_id=user.user_id'
        if (param.lable != '') sql = `${sql} and activity.activity_lable='${param.lable}'`//有分类时
        sql = `${sql}  and (activity.ispublic=1 or activity.ispublic=0)`
        sql = `${sql} order by activity.activity_read_num desc limit ${pagesize}`
        return await this.query(sql)
    }

};
