module.exports = class extends think.Model {

    async gethelplist(param) {
        let sql1 = ' select count(*) as count from help where ispublic=1 or ispublic=0'
        if (param.lable != '') sql1 = `${sql1} and help_lable='${param.lable}'`//有分类时
        if (param.tag != '') sql1 = `${sql1} and help_tag like '%${param.tag}%'`//标签时
        let info1 = []
        return await this.query(sql1, info1)
    }
    async webgethelplist(param) {

        let pagesize = param.pagesize * 1
        let page = (param.page - 1) * pagesize
        let info = [pagesize, page]
        let sql = 'select help.help_id,help.help_title,help.help_read_num, user.nickname from help,user where help.user_id=user.user_id'
        if (param.lable != '') sql = `${sql} and help.help_lable='${param.lable}'`//有分类时
        if (param.tag != '') sql = `${sql} and help.help_tag like '%${param.tag}%'`//标签时
        sql = `${sql}  and help.ispublic=1 or help.ispublic=0`
        sql = `${sql} order by help.help_read_num desc`
        return await this.query(sql, info)
    }

    async gethelpcontent(param) {
        let sql = `update help set help_read_num=help_read_num+1 where help_id = '${param.id}'`
        return await this.query(sql)
    }

    async helpcontent(param) {
        let sql = `select * from help ,user where help.user_id =user.user_id and help_id='${param.id}'`
        return await this.query(sql)  
    }
  
   
};
