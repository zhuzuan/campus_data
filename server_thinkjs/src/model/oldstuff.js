module.exports = class extends think.Model {

    //二手信息列表
    async getweboldstufflist(param) {
        let sql1 = ' select count(*) as count from oldstuff where ispublic=1 or ispublic=0'
        if (param.lable != '') sql1 = `${sql1} and oldstuff_lable='${param.lable}'`//有分类时
        let info1 = []
        return await this.query(sql1, info1)
    }
    async webgetweboldstufflist(param) {
        let pagesize = param.pagesize * 1
        let page = (param.page - 1) * pagesize
        let info = [pagesize, page]
        let sql = 'select * from oldstuff,user where oldstuff.user_id=user.user_id'
        if (param.lable != '') sql = `${sql} and oldstuff.oldstuff_lable='${param.lable}'`//有分类时
        sql = `${sql}  and (oldstuff.ispublic=1 or oldstuff.ispublic=0)`
        sql = `${sql} order by oldstuff.oldstuff_read_num desc limit ${param.pagesize}`
        return await this.query(sql, info)
    }

    //二手信息详情
    async addoldstuffread(param){
        let sql=`update  oldstuff set oldstuff_read_num=oldstuff_read_num+1 where oldstuff_id ='${param.id}'`
        return await this.query(sql)
    }
    async getoldstuffcontent(param){
        let sql = `select * from oldstuff,user where user.user_id=oldstuff.user_id and oldstuff.oldstuff_id='${param.id}'`
        return await this.query(sql)
    }
};
