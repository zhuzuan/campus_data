module.exports = class extends think.Model {


    async articlelable(param) {
        let sql1 = ' select count(*) as count from article  where ispublic=1 or ispublic=0'
        if (param.lable != '') sql1 = `${sql1} and article_lable='${param.lable}'`//有分类时
        let info1 = []
        return await this.query(sql1, info1)
    }

    async getarticlelist(param) {
        let pagesize = param.pagesize * 1
        let page = (param.page - 1) * pagesize
        let info = [pagesize, page]
        let sql = 'select article.article_read_num,article.article_id,article.article_title,article.article_introduction,' +
            'article.article_createtime,user.nickname from article,user where article.user_id=user.user_id '
        if (param.lable != '') sql = `${sql} and article.article_lable='${param.lable}'`//有分类时
        sql = `${sql}  and (article.ispublic=1 or article.ispublic=0)`
        sql = `${sql} order by article.article_read_num desc`
        return await this.query(sql, info)

    }

};
