module.exports = class extends think.Model {

    //获取工作信息
    async joblistcount(param) {
        let sql1 = ' select count(*) as count from job  where ispublic=1 or ispublic=0'
        if (param.lable != '') sql1 = `${sql1} and job_lable='${param.lable}'`//有分类时
        return await this.query(sql1)
    }

    async getjoblist(param) {
        let sql = 'select job.job_id,job.job_name,job.job_createtime,job.job_salary,company.company_name' +
            ' from job,company where job.company_id=company.company_id '
        if (param.lable != '') sql = `${sql} and job.job_lable='${param.lable}'`//有分类时
        sql = `${sql}  and (job.ispublic=1 or job.ispublic=0)`
        sql = `${sql} order by job.job_read_num desc`
        return await this.query(sql)
    }

};
