const Base = require('./base.js');
let data
const s = {
  "type": 'SUCCESS',
  "msg": "操作成功"
}
const e = {
  "type": 'ERROE',
  "msg": "操作失败"
}

module.exports = class extends Base {
  indexAction() {
    return this.display();
  }
//获取问答列表
  async webgetwebhelplistAction() {
    let param = this.post();
    let counts = await this.model("help").gethelplist(param);
    let count = counts[0].count
    let result = await this.model("help").webgethelplist(param);
    if (!think.isEmpty(result || count)) {
      data = {
        state: s,
        data: result,
        count: count
      }
    }
    return this.success(data)
  }
//获取商品列表
  async webgetweboldstufflistAction() {
    let param = this.post();
    let counts = await this.model("oldstuff").getweboldstufflist(param);
    let count = counts[0].count
    let result = await this.model("oldstuff").webgetweboldstufflist(param);
    if (think.isEmpty(result || count)) {
      data = {
        state: e,
      }
    }
    data = {
      state: s,
      data: result,
      count: count
    }
    return this.success(data)
  }
 //获取文章列表
  async getarticlelistAction() {
    let param = this.post();
    let counts = await this.model("article").articlelable(param);
    let count = counts[0].count
    let result = await this.model("article").getarticlelist(param)
    if (!think.isEmpty(count || result)) {
      data = {
        state: s,
        data: result,
        count: count
      }
    }
    return this.success(data);
  }

  async webgetwebactivitylistAction() {
    let param = this.post();
    let counts = await this.model("activity").getwebactivitylist(param)
    let count = counts[0].count
    let result = await this.model("activity").webgetwebactivitylist(param);
    if (!think.isEmpty(count || result)) {
      data = {
        state: s,
        data: result,
        count: count
      }
    }
    return this.success(data)

  }

  async gethelpcontentAction() {
    let param = this.post()
    let result = await this.model("help").helpcontent(param)
    let add = await this.model("help").gethelpcontent(param)
    if (think.isEmpty(result)) {
      data = {
        state: e,
        data: {}
      }
    }
    data = {
      state: s,
      data: result[0]
    }
    return this.success(data)
  }

  async getcommentAction() {
    let param = this.post()
    let counts = await this.model("comment").getcomment(param)
    let count = counts[0].count
    let result = await this.model("comment").webgetcomment(param)
    if (!think.isEmpty(count || result)) {
      data = {
        state: s,
        data: result,
        count: count
      }
    }
    return this.success(data)
  }

  async getoldstuffcontentAction() {
    let param = this.post()
    let add = await this.model("oldstuff").addoldstuffread(param)
    let result = await this.model("oldstuff").getoldstuffcontent(param)
    if (!think.isEmpty(result)) {
      data = {
        state: s,
        data: result
      }
    }
    return this.success(data)
  }
  //获取工作信息
  async webgetjoblistAction() {
    let param = this.post()
    let counts = await this.model("job").joblistcount(param)
    let count = counts[0].count
    let result = await this.model("job").getjoblist(param)
    if (!think.isEmpty(result || count)) {
      data = {
        state: s,
        data: result,
        count: count
      }
    }
    return this.success(data)
  }
  //获取公司信息
  async webgetcompanylistAction() {
    let param = this.post()
    let counts = await this.model("company").companylistcount(param)
    let count = counts[0].count
    let result = await this.model("company").getcompanylist(param)
    if (!think.isEmpty(result || count)) {
      data = {
        state: s,
        data: result,
        count: count
      }
    }
    return this.success(data)
  }
  //获取活动详情
  async getactivitycontentAction() {
    let param = this.post()
    let add= await this.model("activity").activitycontentadd(param)
    
    let result = await this.model("activity").getactivitycontent(param)
    if (!think.isEmpty(result || count)) {
      data = {
        state: s, 
        data: result,
      }
    }
    return this.success(data)
  }
  //获取活动列表
async webgetwebactivitylistAction() {
  let param = this.post()
  let counts = await this.model("activity").webgetwebactivitylistcount(param)
  let count = counts[0].count
  let result = await this.model("activity").webgetwebactivitylist(param)
  if (!think.isEmpty(result || count)) {
    data = {
      state: s,
      data: result,
      count: count
    }
  }
  return this.success(data)
}
};
