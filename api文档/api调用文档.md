## 番知项目后端接口文档

服务器运行url: http://127.0.0.1:8360

请求类型：post



#### 1.登录

登录的接口

|  操作   |                login                 |
| :-----: | :----------------------------------: |
|  方式   |                 post                 |
| url全径 | http://127.0.0.1:8360/webadmin/login |

请求参数

| 序列 |  参数名  |
| :--: | :------: |
|  1   | username |
|  2   | password |

返回数据

| 序列 |   名称   |      返回值       |
| :--: | :------: | :---------------: |
|  1   | 请求状态 |  data.state.type  |
|  2   | 用户信息 | data.data.uerinfo |
|  3   | token值  |  data.data.token  |

#### 2.注册

注册的接口

| 操作 |                 registed                  |
| :--: | :---------------------------------------: |
| 方式 |                   post                    |
| url  | http://127.0.0.1:8360/webadmin/registered |

请求参数

|   参数   | 参数名 |
| :------: | :----: |
| uername  | 用户名 |
| password |  密码  |

返回参数

|  参数名  |      参数       |
| :------: | :-------------: |
| 请求状态 | data.state.type |
| 用户数据 |    data.data    |

#### 3.问答列表

获取问答列表接口

| 操作 |              webgetwebhelplist              |
| :--: | :-----------------------------------------: |
| 方式 |                    post                     |
| url  | http://127.0.0.1:8360/web/webgetwebhelplist |

请求参数

|  参数名  |   参数   |
| :------: | :------: |
| 分类标签 |  label   |
| 问答标签 |   tag    |
| 页面大小 | pagesize |
|   页数   |   page   |

返回参数

|    参数名    |     参数      |
| :----------: | :-----------: |
|   数据个数   |     count     |
|   请求状态   |  data.state   |
| 问答列表数据 |   data.data   |
|    问答id    |    help_id    |
|   问答标题   |  help_title   |
|  问答阅读数  | help_read_num |
|     昵称     |   nickname    |

#### 4.商品列表

获取商品列表接口

| 操作 |              webgetweboldstufflist              |
| :--: | :---------------------------------------------: |
| 方式 |                      post                       |
| url  | http://127.0.0.1:8360/web/webgetweboldstufflist |

请求参数

|  参数名  |   参数   |
| :------: | :------: |
| 分类标签 |  label   |
| 商品总数 |  total   |
| 页面大小 | pagesize |
|   页数   |   page   |

返回参数

|   参数名   |        参数         |
| :--------: | :-----------------: |
|  请求状态  |     data.state      |
|  问答数据  |      data.data      |
|  数据个数  |        count        |
|   商品id   |     oldstuff_id     |
|   用户id   |       uder_id       |
|  商品图片  |    oldstuff_img     |
|  商品名称  |    oldstuff_name    |
|  商品内容  |  oldstuff_content   |
|  商品标签  |   oldstuff_label    |
|  商品热度  | oldstuff_favour_num |
| 商品阅读数 |  oldstuff_read_num  |

#### 5.文章列表

获取文章列表接口

| 操作 |              getarticlelist              |
| :--: | :--------------------------------------: |
| 方式 |                   post                   |
| url  | http://127.0.0.1:8360/web/getarticlelist |

请求参数

|  参数名  |   参数   |
| :------: | :------: |
| 分类标签 |  label   |
| 文章标签 |   tag    |
| 文章总数 |  total   |
| 页面大小 | pagesize |
|   页数   |   page   |

返回参数

|   参数名   |         参数         |
| :--------: | :------------------: |
|  请求状态  |      data.dtate      |
|  请求数据  |      data.data       |
|  文章篇数  |        count         |
|   文章id   |      article_id      |
| 文章阅读数 |   article_read_num   |
|  文章标题  |    article_title     |
|  文章介绍  | article_introduction |
|    昵称    |       nickname       |

#### 6.活动列表

获取活动列表接口

| 操作 |              webgetwebactivitylist              |
| :--: | :---------------------------------------------: |
| 方式 |                      post                       |
| url  | http://127.0.0.1:8360/web/webgetwebactivitylist |

请求参数

|  参数名  |   参数   |
| :------: | :------: |
| 分类标签 |  label   |
| 活动总数 |  total   |
| 页面大小 | pagesize |
|   页数   |   page   |

返回参数

|   参数名   |        参数         |
| :--------: | :-----------------: |
|  请求状态  |     data.state      |
|  活动数据  |      data.data      |
|   活动id   |     activity_id     |
|  活动标题  |   activity_title    |
|   用户id   |       user_id       |
|  活动类型  |    activity_type    |
|  活动照片  |    activity_img     |
|  活动热度  | activity_favour_num |
|  活动内容  |  activity_content   |
| 活动阅读数 |  activity_read_num  |

#### 7.分类列表

获取分类列表接口

| 操作 |               lablelist               |
| :--: | :-----------------------------------: |
| 方式 |                 post                  |
| url  | http://127.0.0.1:8360/admin/lablelist |

请求参数

|  参数名  |    参数    |
| :------: | :--------: |
| 活动名称 | lable_name |

返回参数

|  参数名  |    参数    |
| :------: | :--------: |
| 请求状态 | data.state |
| 分类数据 | data.data  |
|  分类id  |  lable_id  |
| 分类名称 | lable_name |
| 分类列表 |   lable    |

#### 7.轮播图

获取轮播图列表

| 操作 |               carousellist               |
| :--: | :--------------------------------------: |
| 方式 |                   post                   |
| url  | http://127.0.0.1:8360/admin/carousellist |

请求数据

| 参数名 |    参数    |
| :----: | :--------: |
| 标签名 | lable_name |

返回参数

|   参数名   |      参数      |
| :--------: | :------------: |
|  请求状态  |   data.state   |
| 轮播图数据 |   data.data    |
|  轮播图id  |  carousel_id   |
| 轮播图图片 |  carousel_img  |
| 轮播图url  |  carousel_url  |
| 轮播图标题 | carousel_title |

