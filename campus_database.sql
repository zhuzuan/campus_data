/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET SQL_NOTES=0 */;
/*!32312 IF NOT EXISTS*/; 
/*!40100 DEFAULT CHARACTER SET utf8mb4 */;


DROP TABLE IF EXISTS activity;
CREATE TABLE `activity` (
  `activity_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `activity_title` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `activity_lable` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `activity_type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `activity_locale` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `activity_content` longtext COLLATE utf8mb4_bin,
  `activity_img` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `createtime` date DEFAULT NULL,
  `activity_statetime` datetime DEFAULT NULL,
  `activity_endtime` datetime DEFAULT NULL,
  `updatetime` date DEFAULT NULL,
  `activity_favour_num` int(10) DEFAULT NULL,
  `activity_num` int(10) DEFAULT NULL,
  `activity_read_num` int(10) DEFAULT NULL,
  `activity_state` int(10) DEFAULT NULL,
  `activity_istop` int(10) DEFAULT NULL,
  `ispublic` int(10) DEFAULT NULL,
  `activity_impose` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `admin` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS article;
CREATE TABLE `article` (
  `article_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `article_title` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `article_introduction` longtext COLLATE utf8mb4_bin,
  `article_lable` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `article_content` longtext COLLATE utf8mb4_bin,
  `article_createtime` date DEFAULT NULL,
  `article_updatetime` date DEFAULT NULL,
  `article_favour_num` int(25) DEFAULT NULL,
  `article_read_num` int(25) DEFAULT NULL,
  `article_state` int(25) DEFAULT NULL,
  `article_istop` int(25) DEFAULT NULL,
  `ispublic` int(25) DEFAULT NULL,
  `admin` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS carousel;
CREATE TABLE `carousel` (
  `carousel_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '轮播图',
  `carousel_img` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `carousel_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `carousel_createtime` date DEFAULT NULL,
  `carousel_title` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS comment;
CREATE TABLE `comment` (
  `comment_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `content_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `comment_content` longtext COLLATE utf8mb4_bin,
  `comment_createtime` bigint(25) DEFAULT NULL,
  `comment_favour_num` int(25) DEFAULT NULL,
  `comment_state` int(25) DEFAULT NULL,
  `comment_istop` int(25) DEFAULT NULL,
  `ispublic` int(25) DEFAULT NULL,
  `admin` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS company;
CREATE TABLE `company` (
  `company_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `company_scale` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `company_content` longtext COLLATE utf8mb4_bin,
  `company_createtime` date DEFAULT NULL,
  `company_updatetime` date DEFAULT NULL,
  `company_favour_num` int(25) DEFAULT NULL,
  `company_read_num` int(25) DEFAULT NULL,
  `company_state` int(25) DEFAULT NULL,
  `company_istop` int(25) DEFAULT NULL,
  `company_ispublic` int(25) DEFAULT NULL,
  `company_mail` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS help;
CREATE TABLE `help` (
  `help_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `help_title` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `help_tag` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `help_lable` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `help_content` longtext COLLATE utf8mb4_bin,
  `help_img` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `createtime` date DEFAULT NULL,
  `updatetime` date DEFAULT NULL,
  `help_favour_num` int(11) DEFAULT NULL,
  `help_read_num` int(11) DEFAULT NULL,
  `help_state` int(11) DEFAULT NULL,
  `help_istop` int(11) DEFAULT NULL,
  `ispublic` int(11) NOT NULL DEFAULT '1',
  `admin` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`help_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS job;
CREATE TABLE `job` (
  `job_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `company_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `job_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `job_salary` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `job_num` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `job_lable` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `job_content` longtext COLLATE utf8mb4_bin,
  `job_createtime` date DEFAULT NULL,
  `job_updatetime` date DEFAULT NULL,
  `job_favour_num` int(25) DEFAULT NULL,
  `job_read_num` int(25) DEFAULT NULL,
  `job_state` int(25) DEFAULT NULL,
  `job_istop` int(25) DEFAULT NULL,
  `ispublic` int(25) DEFAULT NULL,
  `admin` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS joins;
CREATE TABLE `joins` (
  `join_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `describe` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `content_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `jions_createtime` bigint(25) DEFAULT NULL,
  PRIMARY KEY (`join_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS jubao;
CREATE TABLE `jubao` (
  `jubao_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `jubao_content` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `jubao_user` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `jubao_img` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `jubao_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `jubao_createtime` bigint(25) DEFAULT NULL,
  `jubao_state` int(2) DEFAULT NULL,
  `admin` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `result` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS lable;
CREATE TABLE `lable` (
  `lable_id` char(255) COLLATE utf8mb4_bin NOT NULL,
  `lable_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `lable` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `inputshow` int(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS notice;
CREATE TABLE `notice` (
  `notice_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `user_from` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_to` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `action` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `content_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `router` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `createtime` bigint(25) DEFAULT NULL,
  `state` int(2) DEFAULT NULL,
  `content_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS oldstuff;
CREATE TABLE `oldstuff` (
  `oldstuff_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '二手商品id',
  `user_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户id',
  `oldstuff_img` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '商品图片',
  `oldstuff_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '商品名字',
  `oldstuff_price` int(5) DEFAULT NULL COMMENT '商品价格',
  `oldstuff_content` longtext COLLATE utf8mb4_bin COMMENT '商品内容',
  `createtime` date DEFAULT NULL,
  `oldstuff_lable` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `updatetime` date DEFAULT NULL,
  `oldstuff_favour_num` int(5) DEFAULT NULL,
  `oldstuff_read_num` int(5) DEFAULT NULL,
  `oldstuff_state` int(5) DEFAULT NULL COMMENT '状态',
  `oldstuff_istop` int(5) DEFAULT NULL COMMENT '是否卖出',
  `ispublic` int(5) DEFAULT '1' COMMENT '是否公开',
  `admin` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS reply;
CREATE TABLE `reply` (
  `reply_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `comment_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `reply_content` longtext COLLATE utf8mb4_bin,
  `tousernickname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `touserid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `createtime` bigint(25) DEFAULT NULL,
  `reply_state` int(25) DEFAULT NULL,
  `reply_istop` int(25) DEFAULT NULL,
  `ispublic` int(25) DEFAULT NULL,
  `admin` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS user;
CREATE TABLE `user` (
  `user_id` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `realname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `studentid` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  `studentcard` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `realstate` int(2) DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `qq` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `mail` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `companyname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `companyimg` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `companystate` int(2) DEFAULT NULL,
  `user_state` char(2) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_createtime` bigint(255) DEFAULT NULL,
  `activationdate` bigint(25) DEFAULT NULL,
  `jubao_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_username` (`username`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC;

INSERT INTO activity(activity_id,user_id,activity_title,activity_lable,activity_type,activity_locale,activity_content,activity_img,createtime,activity_statetime,activity_endtime,updatetime,activity_favour_num,activity_num,activity_read_num,activity_state,activity_istop,ispublic,activity_impose,admin) VALUES('1','bbd68950-c9cd-11eb-8149-e9aaa417608f','捐血','志愿公益','公益','本校西区饭堂',X'e78eb0e59cbae68d90e8a180e6b4bbe58aa8',NULL,'2021-06-10','2021-06-17 15:01:38','2021-06-27 15:01:41','2021-06-17',10,300,105,1,0,1,'公益',NULL),('2','bbd68950-c9cd-11eb-8149-e9aaa417608f','防疫志愿者','志愿公益','公益','本校区东西区饭堂',X'e998b2e796abe5bf97e684bfe88085efbc8ce9a5ade5a082e997a8e58fa3e5afb9e68980e69c89e8bf9be587bae4babae59198e8bf9be8a18ce6b58be9878fe4bd93e6b8a9',NULL,'2021-06-13','2021-06-17 15:08:35','2021-07-17 15:08:36','2021-06-17',30,20,308,1,0,1,'公益',NULL);

INSERT INTO article(article_id,user_id,article_title,article_introduction,article_lable,article_content,article_createtime,article_updatetime,article_favour_num,article_read_num,article_state,article_istop,ispublic,admin) VALUES('1','bbd68950-c9cd-11eb-8149-e9aaa417608f','向往的生活',X'e8aeb2e8bfb0e59091e5be80e79a84e7949fe6b4bbefbc8ce4babae4bbace4b880e79bb4e5b88ce69c9be887aae5b7b1e883bde887aae794b1e79a84e98089e68ba9e683b3e8a681e79a84e7949fe6b4bbe696b9e5bc8f','好文',X'e8aeb2e8bfb0e59091e5be80e79a84e7949fe6b4bbefbc8ce4babae4bbace4b880e79bb4e5b88ce69c9be887aae5b7b1e883bde887aae794b1e79a84e98089e68ba9e683b3e8a681e79a84e7949fe6b4bbe696b9e5bc8fefbc8ce4bd86e78eb0e5a682e4bb8ae79a84e7a4bee4bc9ae59fbae69cace5bcbae588b6e79a84e5b086e6af8fe4b8aae4babae983bde99990e588b6e59ca8e4b880e4b8aae789a2e7acbce9878ce38082e983bde683b3e8a681e68ca3e6898ee58db4e58faae79c8be8a781e69bb4e5a4a7e79a84e789a2e7acbce59ca8e5a597e79d80e887aae5b7b1efbc8ce4bb80e4b988e698afe59091e5be80e79a84e7949fe6b4bbefbc8ce68891e79a84e7ad94e6a188e698afe68891e4bbace59ca8e4b880e8b5b7e5b0b1e698afe59091e5be80e79a84e7949fe6b4bbefbc810d0a','2021-06-12','2021-06-13',100,20,1,0,1,NULL),('2','bbd68950-c9cd-11eb-8149-e9aaa417608f','番职好校园',X'e5b9bfe5b79ee795aae7a6bae8818ce4b89ae68a80e69cafe5ada6e999a2','好文',X'e5b9bfe5b79ee795aae7a6bae8818ce4b89ae68a80e69cafe5ada6e999a2e698afe4b880e68980e78eafe5a283e4bc98e7be8eefbc8ce6a0a1e9a38ee889afe5a5bde79a84e8818ce4b89ae9ab98e6a0a1efbc8ce58faae8a681e4bda0e4b880e69da5e5b0b1e4bc9ae788b1e4b88ae79a84e59cb0e696b9efbc8ce59ba0e4b8bae8bf99e9878ce99d9ee5b8b8e98082e59088e585bbe880810d0ae5889de585a5e795aae8818cefbc8ce68891e8bf98e4bba5e4b8bae5a49ae5a4a7e591a2efbc8ce5ae9ee58899e79c9fe79a84e68cbae5a4a7e79a84efbc8ce4b99fe5b0b1e982a3e587a0e69da1e8b7afefbc8ce697a9e5b7b2e8aeb0e5bf86e59ca8e68891e79a84e88491e6b5b7e9878c2ee68891e788b1e8bf99e9878ce79a84e9a5ade5a082e6b2a1e99499efbc8ce4bd86e68891e69bb4e788b1e8bf99e9878ce79a84e8888de58f8b0d0ae59ba0e4b8bae4bb96e4bbace983bde698afe69c89e992b1e4babaefbc8ce59388e59388e59388e59388','2021-06-13','2021-06-17',150,100,1,0,1,NULL);

INSERT INTO carousel(carousel_id,carousel_img,carousel_url,carousel_createtime,carousel_title) VALUES('1','https://scpic.chinaz.net/files/pic/max/201506/max1273.jpg','http://localhost:8080/#/oldstuffcontent/4','2021-06-13','二手台灯'),('2','https://scpic.chinaz.net/files/pic/pic9/202008/apic27336.jpg',NULL,'2021-06-18','志愿活动');


INSERT INTO company(company_id,user_id,company_name,company_scale,company_content,company_createtime,company_updatetime,company_favour_num,company_read_num,company_state,company_istop,company_ispublic,company_mail) VALUES('1','bbd68950-c9cd-11eb-8149-e9aaa417608f','番知小公司','4',X'e68993e6b8b8e6888f','2021-06-10','2021-06-17',100,20,1,0,1,'123123@qq.com');

INSERT INTO help(help_id,user_id,help_title,help_tag,help_lable,help_content,help_img,createtime,updatetime,help_favour_num,help_read_num,help_state,help_istop,ispublic,admin) VALUES('1','22','anan','anan','anan',X'616e616e',NULL,'2021-06-12','2021-06-13',3,4,1,0,1,NULL),('2','543d6940-ce6d-11eb-8b58-e5bf58c2a188','anan','anan','anan',X'616e616e',NULL,'2021-06-12','2021-06-14',1,12,1,0,1,NULL),('3','543d6940-ce6d-11eb-8b58-e5bf58c2a188','拼车滴滴','拼车','互利',X'e7abafe58d88e88a82e5bd93e5a4a9e4b880e8b5b7e68bbce8bda6e58ebbe5b882e6a1a5e59cb0e99381e7ab99',NULL,'2021-06-10','2021-06-11',3,10,1,0,1,NULL);

INSERT INTO job(job_id,user_id,company_id,job_name,job_salary,job_num,job_lable,job_content,job_createtime,job_updatetime,job_favour_num,job_read_num,job_state,job_istop,ispublic,admin) VALUES('1','bbd68950-c9cd-11eb-8149-e9aaa417608f','1','电竞选手','3k/月','1','竞技',X'e5bc80e9bb91e68993e6b8b8e6888f','2021-06-17','2021-06-17',100,100,1,0,1,NULL);



INSERT INTO lable(lable_id,lable_name,lable,inputshow) VALUES('1','问答分类','[\"娱乐\",\"失物认领\",\"学习\",\"寻找资源\",\"其他\"]',0),('2','活动分类','[\"体育\",\"志愿者\",\"学习\"]',0),('3','二手分类','[\"数码\",\"书本\",\"运动器材\",\"电器\"]',0),('4','招聘分类','[\"计算机类\",\"设计类\",\"兼职\"]',0),('5','文章分类','[\"新闻\",\"好文\",\"技术文章\",\"其他\"]',0);


INSERT INTO oldstuff(oldstuff_id,user_id,oldstuff_img,oldstuff_name,oldstuff_price,oldstuff_content,createtime,oldstuff_lable,updatetime,oldstuff_favour_num,oldstuff_read_num,oldstuff_state,oldstuff_istop,ispublic,admin) VALUES('2','5ddcdc30-ce6b-11eb-b0a5-cf85c1dc233e','https://scpic.chinaz.net/files/pic/pic9/201407/apic5288.jpg','二手洗衣机',350,X'e6af95e4b89ae4ba86efbc8ce587bae594aee4ba8ce6898be6b497e8a1a3e69cbaefbc8ce69c89e6848fe88085e69da5e8818a','2021-06-12','电器','2021-06-16',4,13,1,0,1,NULL),('3','5ddcdc30-ce6b-11eb-b0a5-cf85c1dc233e','https://trademark-pics-search.oss-cn-shanghai.aliyuncs.com/big/h4534361759187968.jpg','人生导师',300,X'e4b880e69cace68c87e5bc95e4bda0e4babae7949fe5afbce59091e79a84e4b9a6efbc8ce59388e59388e59388e59388','2021-06-10','书本','2021-06-16',6,28,1,0,1,NULL),('4','5ddcdc30-ce6b-11eb-b0a5-cf85c1dc233e','https://scpic.chinaz.net/files/pic/max/201506/max1273.jpg','台灯',20,X'e5b08fe58fb0e781af','2021-06-15','电器','2021-06-17',2,19,1,0,1,NULL),('5','5ddcdc30-ce6b-11eb-b0a5-cf85c1dc233e','https://scpic.chinaz.net/files/pic/pic9/202009/hpic2964.jpg','篮球',98,X'e4ba8ce6898be7afaee79083','2021-06-17','运动器材','2021-06-17',3,2,1,0,1,NULL);

INSERT INTO user(user_id,username,password,nickname,realname,studentid,studentcard,realstate,avatar,qq,mail,phone,companyname,companyimg,companystate,user_state,user_createtime,activationdate,jubao_id) VALUES('543d6940-ce6d-11eb-8b58-e5bf58c2a188','eee123','eee123456','安仔',NULL,NULL,NULL,1,'https://scpic.chinaz.net/files/pic/pic9/202004/zzpic24628.jpg','14588','14588@qq.com','13456844968',NULL,NULL,1,'2',1623825423699,NULL,NULL),('5ddcdc30-ce6b-11eb-b0a5-cf85c1dc233e','ddd123','ddd123456','该用户还没设置昵称',NULL,NULL,NULL,1,'https://scpic.chinaz.net/files/pic/pic9/202004/zzpic24628.jpg',NULL,NULL,NULL,NULL,NULL,1,'2',1623824580851,NULL,NULL),('82c4e7c0-d10b-11eb-a773-c53ff9178329','ccc123','ccc123456','该用户还没设置昵称',NULL,NULL,NULL,1,'http://oss.guoang.xyz/morentouxiang.jpg',NULL,NULL,NULL,NULL,NULL,1,'2',1624113264572,NULL,NULL),('87ef0360-d102-11eb-a773-c53ff9178329','mmm123','vvv123456','该用户还没设置昵称',NULL,NULL,NULL,1,'http://oss.guoang.xyz/morentouxiang.jpg',NULL,NULL,NULL,NULL,NULL,1,'2',1624109407766,NULL,NULL),('9e155490-ce47-11eb-89ff-b94ec028405f','bbb123','bbb123456','该用户还没设置昵称',NULL,NULL,NULL,1,'https://scpic.chinaz.net/files/pic/pic9/202004/zzpic24628.jpg',NULL,NULL,NULL,NULL,NULL,1,'2',NULL,NULL,NULL),('b13fa5c0-c9c9-11eb-b733-25546cba91d8','zuan124','zuan123457','该用户还没设置昵称',NULL,NULL,NULL,1,'https://scpic.chinaz.net/files/pic/pic9/202004/zzpic24628.jpg',NULL,NULL,NULL,NULL,NULL,1,'2',1623314486023,NULL,NULL),('b5a70970-c9c7-11eb-b41a-c5b7134a90bd','zuan123','f411cd0ac5155a1894b98c1bf748ee56','该用户还没设置昵称',NULL,NULL,NULL,1,'https://scpic.chinaz.net/files/pic/pic9/202004/zzpic24628.jpg',NULL,NULL,NULL,NULL,NULL,1,'2',1623314486023,NULL,NULL),('bbd68950-c9cd-11eb-8149-e9aaa417608f','aaa123','aaa123456','该用户还没设置昵称',NULL,NULL,NULL,1,'https://scpic.chinaz.net/files/pic/pic9/202004/zzpic24628.jpg',NULL,NULL,NULL,NULL,NULL,1,'2',1623317073381,NULL,NULL),('e4f554a0-d0ea-11eb-a773-c53ff9178329','fff123','fff123456','该用户还没设置昵称',NULL,NULL,NULL,1,'http://oss.guoang.xyz/morentouxiang.jpg',NULL,NULL,NULL,NULL,NULL,1,'2',1624099255911,NULL,NULL);